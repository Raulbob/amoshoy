package com.example.amoshoyplane.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.example.amoshoyplane.AppHolder;
import com.example.amoshoyplane.component.BitmapControl;
import com.example.amoshoyplane.GameManager;
import com.example.amoshoyplane.R;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpAppHolder();
    }

    private void setUpAppHolder() {
        Display display = ((WindowManager) getApplicationContext().getSystemService(getApplicationContext().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        setUpAppHolder(displayMetrics);
    }

    private void setUpAppHolder(DisplayMetrics displayMetrics) {
        AppHolder.SCREEN_WIDTH_X = displayMetrics.widthPixels;
        AppHolder.SCREEN_HEIGHT_Y = displayMetrics.heightPixels;

        AppHolder.gravityPull = 5;
        AppHolder.jumpVelocity = -50;

        AppHolder.tubeGap = 650;
        AppHolder.tube_numbers = 2;
        AppHolder.tube_velocity = 24;
        AppHolder.minimumTubeY = AppHolder.tubeGap / 2;
        AppHolder.maximumTubeY = AppHolder.SCREEN_HEIGHT_Y - AppHolder.minimumTubeY - AppHolder.tubeGap;
        AppHolder.tubeDistance = AppHolder.SCREEN_WIDTH_X * 2 / 3;

        AppHolder.bitmapControl = new BitmapControl(getApplicationContext().getResources());
        AppHolder.gameManager = new GameManager();
    }

    public void startGame(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
        finish();
    }

}