package com.example.amoshoyplane.activity;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.amoshoyplane.GamePlay;

/**
 * The activity responsible for the actual game.
 */
public class GameActivity extends Activity {

    private GamePlay gamePlay;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gamePlay = new GamePlay(this);
        setContentView(gamePlay);
    }

}
