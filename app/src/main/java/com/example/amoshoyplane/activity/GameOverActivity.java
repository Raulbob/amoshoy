package com.example.amoshoyplane.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.amoshoyplane.R;

/**
 * Activity responsible with game over screen.
 */
public class GameOverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
    }

}