package com.example.amoshoyplane;

import static com.example.amoshoyplane.constants.GameState.*;

import android.graphics.Canvas;

import com.example.amoshoyplane.constants.GameState;
import com.example.amoshoyplane.pojo.BackgroundImageBean;
import com.example.amoshoyplane.pojo.FlyingBirdBean;
import com.example.amoshoyplane.pojo.TubeBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameManager {

    /**
     * 0 -> game not running
     * 1 -> game running
     * 0 -> game over
     */
    public static GameState gameState;

    private BackgroundImageBean backgroundImageBean;
    private FlyingBirdBean flyingBirdBean;
    private List<TubeBean> tubeCollectionList;

    private Random random;

    public GameManager() {
        backgroundImageBean = new BackgroundImageBean();
        flyingBirdBean = new FlyingBirdBean();
        tubeCollectionList = new ArrayList<>();
        gameState = NOT_STARTED;
        random = new Random();
        generateTubeObject();
    }

    public void generateTubeObject() {
        for (int i = 0; i < AppHolder.tube_numbers; i++) {
            int x = AppHolder.SCREEN_WIDTH_X + i * AppHolder.tubeDistance;
            TubeBean tubeCollection = new TubeBean(x, AppHolder.minimumTubeY);
            tubeCollectionList.add(tubeCollection);
        }
    }

    public void scrollingTube(Canvas canvas) {
        if (gameState.equals(STARTED)) {
            for (int i = 0; i < AppHolder.tube_numbers; i++) {
                TubeBean tube = tubeCollectionList.get(i);
                if (tube.getX() < -AppHolder.bitmapControl.getTubeWidth()) {
                    tube.setX(tubeCollectionList.get(i).getX() + AppHolder.tube_numbers * AppHolder.tubeDistance);
                    int upTubeCollectionY = AppHolder.minimumTubeY + random.nextInt(AppHolder.maximumTubeY - AppHolder.minimumTubeY + 1);
                    tube.setY(upTubeCollectionY);
                }
                tube.setX(tube.getX() - AppHolder.tube_velocity);

                int startOfFirstTubeY = tube.getY() - AppHolder.bitmapControl.getTubeHeight();
                int startOfSecondTubeY = tube.getY() + AppHolder.tubeGap;

                canvas.drawBitmap(AppHolder.bitmapControl.getUpTube(), tube.getX(), startOfFirstTubeY, null);
                canvas.drawBitmap(AppHolder.bitmapControl.getDownTube(), tube.getX(), startOfSecondTubeY, null);
            }
        }
    }

    public void flyingBirdAnimation(Canvas canvas) {
        if (gameState.equals(STARTED)) {
            //gravity
            if (flyingBirdBean.getY() < AppHolder.SCREEN_HEIGHT_Y - AppHolder.bitmapControl.nextBird().getHeight()
                    || flyingBirdBean.getVelocity() < 0) {
                flyingBirdBean.setVelocity(flyingBirdBean.getVelocity() + AppHolder.gravityPull);
                flyingBirdBean.setY(flyingBirdBean.getY() + flyingBirdBean.getVelocity());
            }
        }
        //draw the actual bird
        canvas.drawBitmap(AppHolder.bitmapControl.nextBird(), flyingBirdBean.getX(), flyingBirdBean.getY(), null);
    }

    // Method responsible for the background side scrolling animation
    public void backgroundAnimation(Canvas canvas) {
        backgroundImageBean.setX(backgroundImageBean.getX() - backgroundImageBean.getVelocity());
        if (backgroundImageBean.getX() < -AppHolder.bitmapControl.getBackgroundWidth()) {
            backgroundImageBean.setX(0);
        }
        canvas.drawBitmap(AppHolder.bitmapControl.getBackground(), backgroundImageBean.getX(), backgroundImageBean.getY(), null);

        if (backgroundImageBean.getX() < -(AppHolder.bitmapControl.getBackgroundWidth() - AppHolder.SCREEN_WIDTH_X)) {
            canvas.drawBitmap(AppHolder.bitmapControl.getBackground(), backgroundImageBean.getX() +
                    AppHolder.bitmapControl.getBackgroundWidth(), backgroundImageBean.getY(), null);
        }
    }

    public FlyingBirdBean getFlyingBirdBean() {
        return flyingBirdBean;
    }

}
