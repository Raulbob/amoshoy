package com.example.amoshoyplane.util;

public class Counter {

    private int max;

    private int counter = -1;

    public Counter(int max) {
        this.max = max;
    }

    public int next() {
        counter++;
        if (counter > max) {
            counter = 0;
        }
        return counter;
    }

}
