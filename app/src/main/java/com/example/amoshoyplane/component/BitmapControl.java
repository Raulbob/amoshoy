package com.example.amoshoyplane.component;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.amoshoyplane.AppHolder;
import com.example.amoshoyplane.R;

/**
 * Everything that is drawn in Android is a Bitmap. Allow us to manipulate pixels in the 2d or does it by using an existing photo.
 */
public class BitmapControl {

    private Bitmap background;
    private BirdsFactory birdsFactory;
    private Bitmap upTube;
    private Bitmap downTube;

    public BitmapControl(Resources resources) {
        initBackground(resources);
        initBirdArray(resources);
    }

    private void initBackground(Resources resources) {
        background = BitmapFactory.decodeResource(resources, R.drawable.background);
        background = computeBackgroundScale(background);
        birdsFactory = new BirdsFactory(resources);
    }

    private void initBirdArray(Resources resources) {
        upTube = BitmapFactory.decodeResource(resources, R.drawable.up_tube);
        downTube = BitmapFactory.decodeResource(resources, R.drawable.down_tube);
    }

    public Bitmap getUpTube() {
        return upTube;
    }

    public void setUpTube(Bitmap upTube) {
        this.upTube = upTube;
    }

    public Bitmap getDownTube() {
        return downTube;
    }

    public void setDownTube(Bitmap downTube) {
        this.downTube = downTube;
    }

    public int getTubeWidth() {
        return upTube.getWidth();
    }

    public int getTubeHeight() {
        return upTube.getHeight();
    }

    public void setBackground(Bitmap background) {
        this.background = background;
    }

    public Bitmap getBackground() {
        return background;
    }

    public int getBackgroundWidth() {
        return background.getWidth();
    }

    public int getBackgroundHeight() {
        return background.getHeight();
    }

    /**
     * Makes the computation to brig it to proper scale
     */
    public Bitmap computeBackgroundScale(Bitmap bitmap) {
        float ratio = getBackgroundWidth() / getBackgroundHeight();
        int properScaleWidth = (int) ratio * AppHolder.SCREEN_WIDTH_X;
        return Bitmap.createScaledBitmap(bitmap, properScaleWidth, AppHolder.SCREEN_HEIGHT_Y, false);
    }

    public Bitmap nextBird(int currentFrame) {
        return birdsFactory.next();
    }

    public Bitmap nextBird() {
        return birdsFactory.next();
    }

}
