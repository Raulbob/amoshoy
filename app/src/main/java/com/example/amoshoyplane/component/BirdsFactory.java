package com.example.amoshoyplane.component;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.amoshoyplane.R;
import com.example.amoshoyplane.util.Counter;

/**
 * Class that delivers only 3 Bitmap instances consecutively. Each one representing a state of the bird.
 * We use this to animate the birds flight.
 */
public class BirdsFactory {

    private Bitmap[] birdArray = new Bitmap[3];
    private Counter counter = new Counter(birdArray.length - 1);

    public BirdsFactory(Resources resources) {
        birdArray[0] = BitmapFactory.decodeResource(resources, R.drawable.bird1);
        birdArray[1] = BitmapFactory.decodeResource(resources, R.drawable.bird2);
        birdArray[2] = BitmapFactory.decodeResource(resources, R.drawable.bird3);
    }

    public Bitmap next() {
        return birdArray[counter.next()];
    }

}
