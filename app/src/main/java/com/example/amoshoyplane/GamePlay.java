package com.example.amoshoyplane;

import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.amoshoyplane.constants.GameState;

public class GamePlay extends SurfaceView implements SurfaceHolder.Callback {

    private MainThread mainThread;

    public GamePlay(Context context) {
        super(context);
        SurfaceHolder myHolder = getHolder();
        myHolder.addCallback(this);
        mainThread = new MainThread(myHolder);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
        mainThread.setIsRunning(true);
        mainThread.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        //cand schimbi orientare
        //noi o lasam goala pt ca folosim doar Potrait
    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
        boolean retry = true;
        while (retry) {
            try {
                mainThread.setIsRunning(false);
                mainThread.join();
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        AppHolder.gameManager.gameState = GameState.STARTED;
        AppHolder.gameManager.getFlyingBirdBean().setVelocity(AppHolder.jumpVelocity);
        return true;
    }

}
