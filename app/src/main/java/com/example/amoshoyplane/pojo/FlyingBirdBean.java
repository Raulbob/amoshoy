package com.example.amoshoyplane.pojo;

import android.graphics.Bitmap;

import com.example.amoshoyplane.AppHolder;

public class FlyingBirdBean {

    public static int maximumFrame;

    private int x;
    private int y;
    private int currentFrame;
    private int velocity;

    public FlyingBirdBean() {
        Bitmap drawnBird = AppHolder.bitmapControl.nextBird();
        x = AppHolder.SCREEN_WIDTH_X / 2 - drawnBird.getWidth() / 2;
        y = AppHolder.SCREEN_HEIGHT_Y / 2 - drawnBird.getHeight() / 2;
        currentFrame = 0;
        maximumFrame = 2;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getCurrentFrame() {
        return currentFrame;
    }

    public void setCurrentFrame(int currentFrame) {
        this.currentFrame = currentFrame;
    }

    public int getVelocity() {
        return velocity;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

}
