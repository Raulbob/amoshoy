package com.example.amoshoyplane.pojo;

import com.example.amoshoyplane.AppHolder;

public class TubeBean {

    private int x;
    private int y;

    public TubeBean(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    //unde incepe tubul
    public int getStartOfTube_Y() {
        return y - AppHolder.bitmapControl.getTubeHeight();
    }

    //unde se termina tubul
    public int getDownTube_Y() {
        return y + AppHolder.tubeGap;
    }

}
