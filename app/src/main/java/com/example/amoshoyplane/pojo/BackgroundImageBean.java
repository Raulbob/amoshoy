package com.example.amoshoyplane.pojo;

public class BackgroundImageBean {

    private int x;
    private int y;
    private int velocity;

    public BackgroundImageBean() {
        x = 0;
        y = 0;
        velocity = 4;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getVelocity() {
        return velocity;
    }

    public void setVelocity(int backgroundSpeed) {
        this.velocity = backgroundSpeed;
    }

}
