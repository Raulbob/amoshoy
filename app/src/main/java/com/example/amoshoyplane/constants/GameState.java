package com.example.amoshoyplane.constants;

public enum GameState {

    NOT_STARTED, STARTED, ENDED

}
