package com.example.amoshoyplane;

import com.example.amoshoyplane.component.BitmapControl;

/**
 * Useful properties about the game like size of the screen
 */
public class AppHolder {

    public static int SCREEN_WIDTH_X;
    public static int SCREEN_HEIGHT_Y;

    public static int gravityPull;
    public static int jumpVelocity;

    public static int tubeGap; //opening between top and down tube
    public static int tube_numbers;
    public static int tube_velocity; //the speed at which the tube will keep appearing
    public static int minimumTubeY;
    public static int maximumTubeY;
    public static int tubeDistance;

    public static BitmapControl bitmapControl;
    public static GameManager gameManager;

}
