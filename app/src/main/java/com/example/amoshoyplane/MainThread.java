package com.example.amoshoyplane;

import android.graphics.Canvas;
import android.os.SystemClock;
import android.view.SurfaceHolder;

/**
 * Thread care da Refresh la ecran, cat timp ii flag-ul setat.
 */
public class MainThread extends Thread {

    private SurfaceHolder surfaceHolder;

    private long timeSpent;
    private long kickOffTime;
    private long WAIT = 31;
    private boolean running;
    private static Canvas canvas;

    public MainThread(SurfaceHolder myHolder) {
        this.surfaceHolder = myHolder;
        running = true;
    }

    @Override
    public void run() {
        while (running) {
            kickOffTime = SystemClock.uptimeMillis();
            canvas = null;
            try {
                synchronized (surfaceHolder) {
                    canvas = surfaceHolder.lockCanvas();
                    AppHolder.gameManager.backgroundAnimation(canvas);
                    AppHolder.gameManager.flyingBirdAnimation(canvas);
                    AppHolder.gameManager.scrollingTube(canvas);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            timeSpent = SystemClock.uptimeMillis() - kickOffTime;
            if (timeSpent < WAIT) {
                try {
                    Thread.sleep(WAIT - timeSpent); //Threadul actual ii pe pauza
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void setIsRunning(boolean running) {
        this.running = running;
    }

}
